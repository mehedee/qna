<?php
/**
 * Created by PhpStorm.
 * User: Mehedee Hasan
 * Date: 2/17/14
 * Time: 4:52 AM
 */

class CRUD {
    private $host;
    private $database;
    private $user;
    private $password;
    private $con;
    private $error_string;

    function __construct ($host, $user, $db, $pass){
        $this->host = $host;
        $this->user = $user;
        $this->database = $db;
        $this->password = $pass;

        $this->crud_connect();
    }
    private function crud_connect(){
        $this->con= new mysqli($this->host,$this->user, $this->password, $this->database);
        if($this->con->connect_errno > 0){
            die('Unable to connect to database [' . $this->con->connect_error . ']');
        }
    }

    public function select($table_name, $where, $where_value){
        $sql = $this->con->prepare("SELECT *  FROM `$table_name` WHERE `$where`= ?");
        $sql->bind_param('s',$where_value);
        $sql->execute();
        return "";
    }

    public function insert_user($user_name, $password, $user_email){
        $sql=$this->con->prepare("INSERT INTO `users`(`user_name`, `password`, `user_email`) VALUES ('$user_name', '$password', '$user_email')");
        //$result=$this->con->query($sql);
        $sql->execute();
        return "";
    }

    public function update_user($user_name, $password, $user_email,$where, $where_value){
        $sql=$this->con->prepare("UPDATE `users` SET `user_name`='$user_name',`password`='$password',`user_email`='$user_email' WHERE `$where`=?");
        //$result=$this->con->query($sql);
        $sql=bind_param('s',$where_value);
        $sql->execute();
        return "";
    }
    public function delete_user($table_name,$id_value){
        $sql=$this->con->prepare("DELETE FROM `$table_name` WHERE `id`=?");
        //$result=$this->con->query($sql);
        $sql->bind_param('s',$id_value);
        $sql->execute();
        return "";
    }



}

