<?php
/**
 * Created by PhpStorm.
 * User: Mehedee
 * Date: 2/18/14
 * Time: 5:34 AM
 */

include_once('server_connect.php');

function process_data($data){
    $data= trim($data);
    $data= stripslashes($data);
    $data= htmlspecialchars($data);
    return $data;
}
session_start();

$user_name= process_data($_POST['reg_name']);
$user_email= process_data($_POST['reg_email']);
$password=$_POST['reg_pass'];


$user_exists= $server->select('users','user_name', $user_name);




$email_exists= $server->select('users','user_email', $user_email);


if(!isset($user_name) || !isset($user_email) || !isset($password)){
    $_SESSION['error']['all']="Oops! Looks Like You're In A Hurry But You Must Have to Complete The Form Properly To Continue";
    header('Location: index.php');
}

elseif(strlen($user_name)<5){
    $_SESSION['error']['username']="Sorry, But User Name Can't Be Less Than 5 Characters";
    header('Location: index.php');
}

elseif(strlen($password)<8){
    $_SESSION['error']="Password Have to Be At Least 8 Characters!";
    header('Location: index.php');
}

elseif($user_exists){
    $_SESSION['error']= "Someone Already Registered The Name In The Server, Try a New One";
    header('Location: index.php');
    }

elseif($email_exists){
    $_SESSION['error'] ="<br> This Email Address Is Already Used, Did You Just Forget Your Account ?";
    header('Location: index.php');
}

else {
    $server->insert_user($user_name, md5($password),$user_email);
    $_SESSION['message']='You are registered with the following data <br>' .  $user_name . "<br>" . $user_email . "<br>
    You can now login";
    header('Location: index.php');
}

