<?php
/**
 * Created by PhpStorm.
 * User: Mehedee
 * Date: 2/18/14
 * Time: 5:55 AM
 */

    include('class_server_info.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="css/index.css">
    <link type="text/css" rel="stylesheet" href="css/submit.css">
    <title>Submit A New Question</title>

</head>
<body>

<div class="header">
    <h1>
        Basic Q&A
    </h1>
    <p>
        Helping people with simple problems, easy solutions.
    </p>
</div>

<form action="" method="post">
    <div id="question_container">
        <h3>Submit Your Question</h3>
        <div id="question1">
            <select name="category">
                <option value="">Select Category</option>
            </select> <br><br>
            <select name="os">
                <option value="">Operating System</option>
            </select> <br>
        </div>

        <div id="question2">
            <textarea name="question" rows="8" cols="30" placeholder="Write Down Your Question Here..."></textarea> <br>
            <input type="submit" value="Submit">
        </div>

    </div>
</form>
</body>
</html>